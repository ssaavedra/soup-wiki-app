/*global  */

var server;
var rb = require('react-bootstrap')
  , React = require('react')
  , ReactAsync = require('react-async')
  , when = require('when')
  , url = require('url')
  , browserify = require('browserify');

var log = require('../../lib/log')
  , logger = log.getLogger('http.app.wikita');

if (false && jsdoc) {
var BlobService = require('../../lib/blobService');
var FileService = require('../../lib/fileService');
var ClaimService = require('../../lib/claimService');
}

// Transparently require jsx files
require('node-jsx').install();

var Root = require('./templates/base.jsx');

/**
 * @constructor
 * Wikita
 *
 * @param {string} prefix - Prefix of this application for correct
 *                          routing between pages.
 */
var Wikita = function (prefix) {

  /**
   * The prefix of this application for correct routing between pages.
   */
  this.prefix = prefix;

  /**
   * @member {BlobService}
   */
  this.blobService = null;

  /**
   * @member {FileService}
   */
  this.fileService = null;

  /**
   * @member {ClaimService}
   */
  this.claimService = null;

  /**
   * @member {PermanodeService}
   */
  this.permanodeService = null;
};

/**
 * @constructor
 * Wikita API
 *
 * @param {Wikita} wikita - The wikita instance
 */
Wikita.prototype.api = function (wikita) {
  this.w = wikita;

  this.cookies = {};
};

/**
 * Gets the type of an identifier, so that we can route accordingly
 * and present disambiguation pages and so on.
 */
Wikita.prototype.api.prototype.getType = function (identifier) {
  if (identifier.indexOf("sha1") == -1) {
    return when("search-by-claim");
  }
  return this.w.blobService.getType(identifier).catch (function (e) {
	   return "notfound";
	 });
};


/**
 * Wikita API endpoint for /api/1/get/:wikipage requests.
 *
 * @param {arguments} args - The arguments received by the router.get
 * @param {http.ClientRequest} req
 * @param {http.ServerResponse} res
 * @returns {when.Promise} promise that will trigger the rendering of
 * the response.
 */
Wikita.prototype.api.prototype.get = function (args, req, res) {
  var wikipage = args[0];

  var type = this.getType (wikipage);

  var response = type.then (function (type)
  {
    res.setHeader("X-BlobType", type);

    if (type == "search-by-claim") {
      // It is a diambig. page or a redirection
      return this.get_search(wikipage, req, res);

    } else if (type == "file") {
      // It is a raw file. Find permanodes/claims with it
      return this.get_file(type, wikipage, req, res);

    } else if (type == "claim") {
      // It may be an old version of a permanode
      return this.get_by_claim(wikipage, req, res);

    } else if (type == "permanode") {
      // It is a permanode. Find latest file via getBytes directly
      return this.get_file(type, wikipage, req, res);

    } else if (type == "notfound") {
      // We didn't found anything, return 404
      return this.get_error(404, wikipage, req, res);

    } else {
      // Catch-all that should never happen, return 500
      return this.get_error(500, wikipage, req, res);
    }
  }.bind(this)).catch (function (e) {
			 res.writeHead (500);
			 res.end (JSON.stringify({status: 500, error: e.message}));
		       });;

  return response.then (function (response) {
	   res.setHeader ("Content-Type", "application/json");

           if (response.status === 302 && response.location) {
             res.setHeader ("Location", response.location);
           }
	   res.writeHead (response.status);
	   res.end (JSON.stringify(response));
	   return response;
	 });

  var length = this.w.fileService.getLength (wikipage)
	       .then (function (length) {
			res.setHeader ("Content-Length", length);
		      });

  var content = length.then (function () {
		  return this.w.fileService.getBytes (wikipage);
		}.bind (this));

  when.all ([content, type]).then(function (v) {
    var content = v[0],
	type = v[1];

    res.setHeader ("X-BlobType", type);
    res.end(content);
  }).catch (function (e) {
	      console.log ("Something happened", e.stack);
	      res.writeHead (404);
	      res.end(JSON.stringify({status:404, text:"No wiki page"}));
	    });

};


/**
 * Gets a wiki page, or a disambiguation page from a search-by-name claim
 */
Wikita.prototype.api.prototype.get_search = function (wikipage, req, res) {
  var p = this.w.claimService.findByName (wikipage);

  return p.then (function (claims) {
    if (claims.length == 0) {
      // Present no search results page
      return {
	status: 404,
	title: "Not found",
	text: "Page not found",
	type: "not-found",
        results: []
      };

    } else if (false && claims.length == 1) {
      // TODO Return redirection
      var permanode = claims[0].claim.permanode;

      return this.get_file ("permanode", permanode, req, res).then (function (t) {
               t.canonical = permanode;
               return t;
             });
    } else {
      // Disambig./search results page
      claims = claims.map(function (e) { return {
        permanode: e.claim.permanode,
        claim_id: e.claim.id,
        date: e.claim.date,
        issuer: e.claim.issuer,
        signatures: e.toJSON().meta.signatures
      } });
      console.log ("RESULTS PAGE HAVE CLAIMS = ", claims);

      return {
	status: 300,
	text: wikipage,
	title: wikipage,
	type: "claim",
	results: claims
      };
    }

  }.bind (this));
};

var by_date = function (a, b) {
  return a.content.date < b.content.date ? -1
       : (a.content.date > b.content.date ? 1 : 0);
};

Wikita.prototype.api.prototype.getTitle = function (permanode_id) {
  return this.w.claimService.getByPermanode (permanode_id, 'title')
	 .then (function (claims) {
	   claims = claims.map (function (i) { return i.claim; });
	   console.log ("Getting title = ", claims);
           if (claims.length === 0) {
             return "No title";
           }
	   return claims.sort(by_date)[0].value.value;
	 });
};

Wikita.prototype.api.prototype.get_by_claim = function (claim_id, req, res) {
  // Get the claim
  var $claim,
      $title;

  return this.w.claimService.getById (claim_id).then (function (claim) {
	   $claim = claim = claim.claim;
	   if (claim.type !== "set-attribute") {
	     throw new TypeError("Invalid claim to ask for!");
	   }

	   return this.getTitle (claim.permanode).then (function (title) {
	     $title = title;
	     return claim.value.value;
	   });

	 }.bind(this)).then (function (file_id) {
		    var o = this.get_file ("claim", file_id);
		    o.permanode = $claim.permanode;
		    o.claim = $claim.id;
		    o.title = $title;
		    return o;
		  }.bind (this));
};

Wikita.prototype.api.prototype.get_file = function (type, wikipage) {
  var length = this.w.fileService.getLength (wikipage);
  var content = this.w.fileService.getBytes (wikipage);
  var title = when ("No title");

  if (type === "permanode") {
    title = this.getTitle (wikipage);
  }

  return when.all([length, content, title]).then (function (args) {
	   var length = args[0],
	       content = args[1],
	       title = args[2];

	   console.log ("Got content", content.toString());
	   return {status: 200, title: title, text: content.toString(), type: type, permanode: wikipage};
	 }).catch (function (e) {
		     return {status: 500, title: "Error", text: e.stack};
		   });;

};

Wikita.prototype.api.prototype.history = function (args, req, res) {
  var wikipage = args[0], $permanode;

  return this.getType (wikipage).then (function (type) {
	   if (type === "claim") {
	     return this.w.claimService.getById (wikipage).then (function (claim) {
		      return claim.claim.permanode;
		    });
	   } else if (type === "file") {
	     throw new TypeError("Cannot resolve history for a filecontent");
	   }

	   return wikipage;
	 }.bind (this))
	 .then (function (permanode_id) {
	   $permanode = permanode_id;
	   return permanode_id;
	 })
	 .then (this.w.claimService.getAllByPermanode.bind (this.w.claimService))
	 .then (function (claims) {
	   return claims.filter (function (claim) {
		    return claim.claim.type === "set-attribute"
			&& ( claim.claim.value.attribute === "content"
			  || claim.claim.value.attribute === "title");
		  });
	 }).then (function (claims) {
	   res.setHeader ('Content-Type', 'application/json');
	   res.writeHead (200);
	   res.end (JSON.stringify({"status": 200, permanode: $permanode, claims: claims.map(function (claim) { return claim.toJSON(); })}));
	 });
};

Wikita.prototype.api.prototype.get_error = function (err_code, wikipage, req, res) {
  res.setHeader ("X-Requested-Wikipage", wikipage);
  return {status: err_code, text: "Error while retrieving " + wikipage};
};


Wikita.prototype.api.prototype.edit = function (args, req, res) {
  var wikipage = args[0],
      fullBody = req.body,
      new_content = fullBody.content,
      filename = "wikifile",
      buffer = new Buffer (new_content),
      $is_permanode = false,
      $content_id;

  this.init_cookies(req);
  var identity = this.get_cookie ('identity');

  this.getType (wikipage).then (function (type) {
    if (type === "permanode" && !identity) {
      res.writeHead (403);
      res.end (JSON.stringify({status: 403, text: "Please select an identity before editing permanodes"}));
      throw null;
    }

    if (type === "permanode") {
      $is_permanode = true;
    }
  }).then (this.w.fileService
	   .uploadFile.bind(this.w.fileService,
			    buffer,
			    filename,
			    null))
  .then (function (file) {
	   $content_id = file.id;
	   if ($is_permanode) {
	     console.log ("UPLOADING CLAIM: FOR: ", wikipage, $content_id, identity);
	     // Upload a new claim
	     return this.w
		    .claimService.setContent (wikipage, $content_id, identity)
		    .then(function (claim) {
		      console.log (claim);
		    });
	   }
	   return file._id;
	 }.bind (this))
  .then (function (id) {
    res.setHeader('Location', id);
    res.writeHead(204);
    res.end(JSON.stringify({status: 204, location: id}));
  });

  /*.then (function (file) {
    return claimService.setContent (permanode, file._id, identity_id);
    });*/
};

Wikita.prototype.api.prototype.setTitle = function (args, req, res) {
  var wikipage = args[0],
      fullBody = req.body,
      title = fullBody.title,
      $is_permanode = false;

  this.init_cookies(req);
  var identity = this.get_cookie ('identity');

  this.getType (wikipage).then (function (type) {
    if (type === "permanode" && !identity) {
      res.writeHead (403);
      res.end (JSON.stringify({status: 403, text: "Please select an identity before editing permanodes"}));
      throw null;
    }

    if (type !== "permanode") {
      res.writeHead (400);
      res.end (JSON.stringify({status: 400, text: "You can only set titles on permanodes"}));
      throw null;
    }
  }).then (this.w.claimService.setName.bind (this.w.claimService, wikipage, title, identity))
  .then (function (claim) {
    res.writeHead(200);
    res.end(JSON.stringify({status: 204, claim: claim, title: title}));
  });
};


Wikita.prototype.api.prototype.create = function (args, req, res) {
  var fullBody = req.body,
      content = fullBody.content,
      title = fullBody.title,
      filename = "wikifile",
      buffer = new Buffer (content),
      $is_permanode = true,
      $content_id;

  this.init_cookies(req);
  var identity = this.get_cookie ('identity');

  // First, create the idea's permanode
  var p_pid = this.w.permanodeService.create (identity).then (function (permanode) {
    return permanode.id;
  });

  var p_title = p_pid.then (function (permanode_id) {
                  return this.w.claimService.setName (permanode_id, title, identity);
                }.bind (this));

  var p_file = this.w.fileService.uploadFile (buffer, filename, null);

  when.all ([p_pid, p_file, p_title]).then (function (values) {
    var permanode_id = values[0]
      , file_id = values[1].id;

    return [permanode_id, this.w.claimService.setContent (permanode_id, file_id, identity)];
  }.bind (this))
  .then (function (values) {
    var id = values[0]
      , claim_id = values[1].id;

    res.setHeader('Location', '../' + id);
    res.setHeader("Content-Type", "application/json");
    res.writeHead(200);
    res.end(JSON.stringify({status: 200, location: id, claim: claim_id}));
  });
};

Wikita.prototype.api.prototype.sign_claim = function (args, req, res) {
  var fullBody = req.body,
      claim_id = fullBody.claim;

  this.init_cookies (req);
  var identity = this.get_cookie ('identity');

  return this.w.claimService.sign (claim_id, identity).then (function (data) {
           res.setHeader("Content-Type", "application/json");
           res.writeHead(200);
           res.end(JSON.stringify({ status: 200, data: data }));
           return { status: 200, data:data };
         }).catch (function (exn) {
           res.setHeader("Content-Type", "application/json");
           res.writeHead(400);
           res.end(JSON.stringify({ status: 400, message: exn.message }));
         });

};


/**
 * @param {string} api_endpoint - API point to defer access to
 */
Wikita.prototype.api_defer = function (api_endpoint) {
  var api_controller = new (this.api)(this);
  return function () {
    var req = this.req,
	res = this.res;

    return api_controller[api_endpoint](arguments, req, res);
  };
};

Wikita.prototype.defer_controller = function () {
  var controller = new (this.controller)(this.prefix);
  return function () {
    var req = this.req,
	res = this.res;

    return controller.handler(req, res);
  };
};

/**
 * @constructor {WikitaController}
 * WikitaController
 *
 * @param {string} prefix - Prefix of this application for correct
 *                          routing between pages.
 *
 */
Wikita.prototype.controller = function (prefix) {

  this.prefix = prefix;

  /**
   * @member {{string:string}}
   */
  this.cookies = {};
};

Wikita.prototype.controller.prototype.init_cookies = function (req) {

  if (!req.headers.cookie) {
    this.cookies = {};
    return;
  }
  var cookie_list = req.headers.cookie.split('; ');

  this.cookies = {};

  for (var i = 0, _l = cookie_list.length; i < _l; i++) {
    var kv = cookie_list[i].split('=');
    this.cookies[kv[0]] = kv[1];
  }

};

Wikita.prototype.controller.prototype.get_cookie = function (name) {
  return this.cookies[name];
};

Wikita.prototype.api.prototype.init_cookies = Wikita.prototype.controller.prototype.init_cookies;
Wikita.prototype.api.prototype.get_cookie = Wikita.prototype.controller.prototype.get_cookie;

Wikita.prototype.controller.prototype.ext_routes = function () {
  return {
    "app.wikita.api.v1.get": "http://127.0.0.1:8124" + this.prefix + "/api/1/get/:1",
    "app.wikita.api.v1.new": "http://127.0.0.1:8124" + this.prefix + "/api/1/create",
    "app.wikita.api.v1.edit": "http://127.0.0.1:8124" + this.prefix + "/api/1/edit/:1",
    "app.wikita.api.v1.sign-claim": "http://127.0.0.1:8124" + this.prefix + "/api/1/sign-claim/:1",
    "app.wikita.api.v1.edit.title": "http://127.0.0.1:8124" + this.prefix + "/api/1/set-title/:1",
    "app.wikita.api.v1.history": "http://127.0.0.1:8124" + this.prefix + "/api/1/history/:1"
  };
};


/**
 * This is the front controller for the server-side rendering.
 */
Wikita.prototype.controller.prototype.handler = function (req, res)
{
  this.init_cookies(req);

  var prefix = this.prefix;
  var identity = this.get_cookie ('identity');
  var controller = this;

  // Maybe redirect
  var purl = url.parse(req.url);

  var Router = require('rrouter');
  var fetchViews = require('rrouter/lib/fetchViews')
  var PathnameRouting = require('rrouter/lib/routing/PathnameRouting')
  var routes = require('./templates/routes.jsx');

  routes = Router.Routes(null,
			 Router.Route({ path: prefix },
				      routes));

  var match = Router.matchRoutes(routes, purl.pathname);

  fetchViews(match).then (function (match) {
    var context = { match: match,
		    routing: new PathnameRouting(routes),
		    routes: routes,
		    prefix: prefix,
		    ext_routes: controller.ext_routes()
		  };

    React.withContext (context, function () {
      var app = Root({ path: purl.pathname,
		       prefix: prefix,
		       initialPath: purl.pathname,
		       initialIdentity: identity,
		       ext_routes: controller.ext_routes()
		     },
		     Router.createView(match));

      logger.debug ("Rendered:", purl.pathname, prefix);

      var content = ReactAsync.renderComponentToStringWithAsyncState(app, function(err, markup, data) {
		      if (err) {
			logger.warn ("ERR HAPPENED", err, err.stack);
		      }
		      res.end(ReactAsync.injectIntoMarkup(markup, data, [prefix + '/assets/bundle.js']));
		    });
    });
  });
  return;

};


exports.initialize = function (config, router, _prefix, loader) {

  var wikita = new Wikita(_prefix);

  loader('blobService').then(function (s) { wikita.blobService = s; });
  loader('fileService').then(function (s) { wikita.fileService = s; });
  loader('claimService').then(function (s) { wikita.claimService = s; });
  loader('permanodeService').then(function (s) { wikita.permanodeService = s; });

  var callback = wikita.defer_controller();

  router.get('/', callback);
  router.get('/_new', callback);
  router.get('/([^_][-A-Za-z0-9]+)', callback);
  router.get('/_edit/:wikipage', callback);
  router.get('/_search/', function () {
    var req = this.req, res = this.res;

    var qs = url.parse (req.url, true).query;

    var query = qs.q;

    if (!query) {
      res.writeHead(302, { "Location": "../not-found/" });
      res.end("");
      return;
    }

    res.writeHead(302, { "Location": _prefix + "/_search/" + query });
    res.end(JSON.stringify(qs));
  });
  router.get('/_search/:wikipage', callback);
  router.get('/_alt/:wikipage', callback);
  router.get('/_history/:wikipage', callback);

  router.post('/api/1/create', wikita.api_defer('create'));
  router.get('/api/1/get/:wikipage', wikita.api_defer('get'));
  router.get('/api/1/history/:wikipage', wikita.api_defer('history'));
  router.post('/api/1/edit/:wikipage', wikita.api_defer('edit'));
  router.post('/api/1/sign-claim/:claimId', wikita.api_defer('sign_claim'));
  router.post('/api/1/set-title/:wikipage', wikita.api_defer('setTitle'));

  router.get('/assets/bundle.js', function () {
    var res = this.res;
    var bundle_location = __dirname + "/client_bundle.cached.js";
    var fs = require('fs');

    var stats = fs.statSync(bundle_location);
    if (stats.isFile()) {
      fs.readFile(bundle_location, function (err, data) {
	if (!err) {
	  res.end (data);
	}
      });
    } else {
      var b = browserify(__dirname + '/client', {debug: true, watch: false})
      b.require(__dirname + '/node_modules/react/react.js', {expose: 'react'})
      .bundle(function(err, buf) {
	if (err) {
	  console.log ("Hello, error", err);
	  res.writeHead(500);
	  return res.end("/*Error happened*/");
	}
	res.writeHead (200);
	return res.end (buf);
      });
    }
  });

  router.get('/static/(.*)?', function (path) {
    var fs = require('fs');
    var req = this.req
      , res = this.res
      , context = this;

    if (path.indexOf('..') !== -1) {
      res.writeHead (403, {"Content-Type": "application/json"});
      res.end(JSON.stringify({"status": 403, "message": "Path " + path + " forbidden"}));
    }
    var safe_file_location = __dirname + '/static/' + path;

    fs.readFile(safe_file_location, function (err, data) {
      if (err) {
	res.writeHead(404);
	return callback.call(context);
      }
      return res.end(data);
    });

  });

};
