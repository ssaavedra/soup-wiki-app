/** @jsx React.DOM */
var React = require('react')
  , ReactAsync = require('react-async')
  , bs = require('react-bootstrap')
  , marked = require('marked')
  , superagent = require('superagent')
  , WikiContentLoaderMixin = require('./wikiContentLoaderMixin')
  , WikiTabs = require('./wikitabs.jsx')
  , ClaimGuardMixin = require('./claimguardmixin.jsx')
;

var ButtonToolbar = bs.ButtonToolbar
  , Button = bs.Button
  , Glyphicon = bs.Glyphicon
;


var TitleEditor = React.createClass({
  propTypes: {
    initialTitle: React.PropTypes.string,
    handleEdit: React.PropTypes.func
  },

  getInitialState: function () {
    return { title: this.props.initialTitle, editing: false, sending: false };
  },

  handleEdit: function (e) {
    this.setState({editing: true});
  },

  handleAbort: function (e) {
    this.setState({editing: false});
  },

  handleSubmit: function (e) {
    var value = this.refs.edit.getDOMNode().value;

    if (this.props.handleEdit) {
      this.setState({sending: true});
      this.props.handleEdit(e, value);
    }
  },

  componentWillReceiveProps: function (nextProps) {
    if ("initialTitle" in nextProps && nextProps.initialTitle !== this.props.initialTitle) {
      this.setState({editing: false, title: nextProps.initialTitle, sending: false});
    }
  },

  render: function () {
    if (this.state.editing) {
      return this.renderEditor();
    } else {
      return this.renderTitle();
    }

  },

  renderEditor: function () {
    var isLoading = this.state.sending;

    return (<span>
	    <input type="text" ref="edit" placeholder={this.state.title} />
	    <Button onClick={isLoading ? null : this.handleSubmit} disabled={isLoading}>
	      <Glyphicon glyph="ok" />
	    </Button>
	    <Button onClick={isLoading ? null : this.handleAbort} disabled={isLoading}>
	      <Glyphicon glyph="remove" />
	    </Button>
	    </span>);
  },

  renderTitle: function () {
    return (<span>{this.state.title} <Button bsSize="xsmall" onClick={this.handleEdit}>edit</Button></span>);
  }
});



module.exports = React.createClass({
  mixins: [ReactAsync.Mixin, WikiContentLoaderMixin, ClaimGuardMixin],

  getInitialStateAsync: function (cb) {
    this.getWikiContent (this.props.wikipage, cb);
  },

  getInitialState: function () {
    return { };
  },

  handleChange: function () {
    this.setState ({value: this.refs.textarea.getDOMNode().value});
  },

  handleSubmit: function (e) {
    this.setWikiContent (this.props.wikipage, this.state.value, function (x) {
	console.log ("SUCCESSFUL POST");
	console.log ("Got ", x, " in return");
    });
    console.log ("Should submit now, but cannot", e);
    console.log ("Our new content is", this.state.value);

    e.preventDefault();
  },

  handleTitleEdit: function (e, new_title) {
    console.log ("GOT to change title!");
    this.setWikiTitle (this.props.wikipage, new_title, function (x) {
      console.log ("OK");
      console.log ("Got the following xhr", x);
      this.setState ({title: new_title});
    }.bind(this));
  },

  propTypes: {
    component: React.PropTypes.component,
    componentProps: React.PropTypes.object
  },

  getDefaultProps: function () {
    return {
      component: React.DOM.div,
      componentProps: {}
    };
  },

  renderView: function () {
    return this.props.component(this.props.componentProps, (
      <div className="MarkdownEditor">
      <WikiTabs wikipage={this.props.wikipage} tab="edit" />
        <h1>Editing <TitleEditor initialTitle={this.state.title} handleEdit={this.handleTitleEdit} /></h1>
        <h3><small>({this.props.wikipage})</small></h3>
      <form action={"/app/wiki/_edit/" + this.props.wikipage} method="post">
        <textarea
          onChange={this.handleChange}
          ref="textarea"
          value={this.state.value} />

        <ButtonToolbar>
          <Button onClick={this.handleSubmit}>Edit</Button>
        </ButtonToolbar>
      </form>
        <h3>Preview</h3>
        <div
          className="content"
          dangerouslySetInnerHTML={{__html: marked(this.state.value || "")}} />
      </div>));
  }
});
