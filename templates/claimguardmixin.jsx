/** @jsx React.DOM */
var React = require('react')
  , ReactAsync = require('react-async')
  , bs = require('react-bootstrap')
  , WikiContentLoaderMixin = require('./wikiContentLoaderMixin')
  , Link = require('rrouter/lib/Link')

  , Alert = bs.Alert
  , Button = bs.Button

  , SearchResults = require('./searchresults.jsx')
;

var RoutingContextMixin = require('rrouter/lib/RoutingContextMixin');


var ClaimGuardMixin = module.exports = {
  mixins: [RoutingContextMixin],

  propTypes: {
    type: React.PropTypes.string,
    hide: React.PropTypes.array
  },

  render: function () {

    if (this.state.status === 404) {

      return (<div>
        <Alert bsStyle="warning">
          <h4>Error, page not found!</h4>
	  <p>
	    The page you requested does not exist as such. The button below will take you to a search for the keyword {this.props.wikipage}.
	  </p>
          <p>
            <Button bsStyle="danger" href={this.getRouting().makeHref("/search", {wikipage: this.props.wikipage})}>Use the search</Button>
          </p>
        </Alert>
	</div>
      );

    }

    if (this.state.status === 300 && this.state.type === "claim") {
      return (<div>
	<Alert bsStyle="warning">
	<h4>Error, page not found</h4>
	<p>We did not find the page you were looking for, but these items are close enough so that you should take a look.</p>
	</Alert>
	<SearchResults wikipage={this.props.wikipage} />
	</div>);
    }

    return this.renderView();
  }
};
