/** @jsx React.DOM */
var React = require('react')
  , ReactAsync = require('react-async')
  , bs = require('react-bootstrap')
  , marked = require('marked')
  , superagent = require('superagent')
  , WikiContentLoaderMixin = require('./wikiContentLoaderMixin')
  , WikiTabs = require('./wikitabs.jsx');

var ButtonToolbar = bs.ButtonToolbar
  , Button = bs.Button
  , Input = bs.Input
  , rr = require('rrouter')
;

var Link = rr.Link;


module.exports = React.createClass({
  mixins: [WikiContentLoaderMixin, ReactAsync.Mixin],

  propTypes: {
    component: React.PropTypes.component,
    componentProps: React.PropTypes.object
  },

  getDefaultProps: function () {
    return {
      component: React.DOM.div,
      componentProps: {}
    };
  },

  getInitialStateAsync: function (cb) {
    this.getWikiContent (this.props.wikipage, function (err, content) {
      console.log ("GOT INITIAL STATE ASYNC = ", content, content.claims, err);
      cb.apply (this, arguments);
    });
  },

  componentWillReceiveProps: function (nextProps) {
    this.getInitialStateAsync (function (ok, nextState) {
      console.log ("SET NEW STATE, ", nextState);
      this.setState (nextState);
    }.bind (this));
  },

  render: function () {

    var results = (<li>"Hello world"</li>);

    if (!this.state || !this.state.results) {
      this.state = { results: [] };
    }

    var pretty_hash = function (s) {
      if(!s) s = "";
      return s.substring(5, 12);
    }

    var results = this.state.results.map (function (result) {
      return (<li>
	<Link to="/view" wikipage={result.permanode}>{pretty_hash(result.permanode)}</Link> created at {result.date} by {pretty_hash(result.issuer)}</li>);
    });

    var list_of_results = React.DOM.ul.bind (null, null).apply (null, results);


    if (results.length === 0) {
      list_of_results = (<div>No results found, please try another query.</div>);
    }

    return this.props.component(this.props.componentProps, (
      <div>
        <h1>"{this.state.title}" can refer to:</h1>

      {list_of_results}
      </div>));
  }
});
