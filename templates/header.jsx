/** @jsx React.DOM */
var React = require('react')
  , ReactAsync = require('react-async')
  , bs = require('react-bootstrap')
  , superagent = require('superagent')
;

var Navbar = bs.Navbar
  , Nav = bs.Nav
  , NavItem = require('./NavItem')
  , MenuItem = bs.MenuItem
  , Modal = bs.Modal
  , OverlayMixin = bs.OverlayMixin
  , Button = bs.Button
  , Input = bs.Input
  , Glyphicon = bs.Glyphicon
  , DropdownButton = bs.DropdownButton
;

var LinkMixin = require('rrouter/lib/LinkMixin');
var RoutingContextMixin = require('rrouter/lib/RoutingContextMixin');

var THEGLOBALS = require('./globals');


var Identities = React.createClass({
  mixins: [ReactAsync.Mixin],

  propTypes: {
    onSelect: React.PropTypes.func
  },

  getInitialStateAsync: function (cb) {
    var chain = superagent.get('http://127.0.0.1:8124/id/sign')
    .set('Accept', 'text/plain')

    if (chain.buffer === undefined) chain.buffer = function(){return this;};

    chain.buffer()
    .end(function (res) {
      if (res.ok) {
	console.log ("GOT FROM XHR", res.text);
	cb (undefined, { identities: JSON.parse(res.text) });
      } else {
	console.log ("ERROR HAPPENED", res);
	cb (res.text, { identities: [], identities_err: res.text });
      }
    });
  },

  selectIdentityHandler: function (item) {
    return function (e) {
      if (this.props.onSelect)
	this.props.onSelect (item);

      if (e)
	e.preventDefault();
    }.bind(this);
  },

  render: function () {

    var identities = ["Loading identities..."];

    if (this.state.identities) {
      identities = this.state.identities;
    }

    identities = identities.map (function (item) {
      return MenuItem({key: item,
	onClick: this.selectIdentityHandler (item)
      }, item.slice (5, 12));
    }.bind (this));

    return DropdownButton.apply (null, [{title: "Identity",
      ref: "id_selector"}].concat (identities));

  }

});

var UserNav = React.createClass({

  mixins: [OverlayMixin],

  propTypes: {
    initialIdentity: React.PropTypes.string
  },

  getDefaultProps: function () {
    return { initialIdentity: null };
  },

  getInitialState: function () {
    return { identity: this.props.initialIdentity, isModalOpen: false };
  },

  handleOpenLoginModal: function (e) {
    this.setState({
      isModalOpen: !this.state.isModalOpen
    });

    if(e)
      e.preventDefault();
  },

  handleLoginModal: function (id) {
    if (typeof document !== "undefined" && "cookie" in document) {
      var date = new Date();
      var days = 30;
      date.setTime(date.getTime()+(days*24*60*60*1000));
      document.cookie = "identity="+id+"; expires="+date.toGMTString() + "; path=/app/wiki";
      console.log("CHANGINGCOOKIE", document.cookie);
    }
    this.setState({identity: id}, function () {
      console.log ("State mutated!", this.state);
    });
    console.log ("Set state: ", id, this.state);
    this.handleOpenLoginModal();
  },

  handleLogout: function (e) {
    this.setState({identity: null});
    var date = new Date();
    var days = 30;
    date.setTime(date.getTime()+(days*24*60*60*1000));
    document.cookie = "identity=; expires="+date.toGMTString() + "; path=/app/wiki";
    e.preventDefault();
  },

  renderOverlay: function () {
    if (!this.state.isModalOpen) {
      console.log ("Overlay is not open");
      return <span/>;
    }

    return (
      <Modal title="Choose identity" onRequestHide={this.handleOpenLoginModal}>
      <div className="modal-body">
      <form action="#">
      <Identities onSelect={this.handleLoginModal} />
      </form>
      </div>
      <div className="modal-footer">
	<Button onClick={this.handleOpenLoginModal}>Close</Button>
      </div>
      </Modal>
      );
  },


  render: function () {

    var userstyle = { display: "none" };
    var loginstyle = {};

    if (this.state.identity) {
      loginstyle = userstyle;
      userstyle = {};
    }

    if (this.state.identity) {
      THEGLOBALS.identity = this.state.identity;
      console.log("Set Context/ID to State/ID");
    }

    console.log ("Rendering userbar with identity", this.state.identity);

    return (
      <Nav className="navbar-right" navbar={true}>
      <NavItem key={3} ref="lout" href="#" style={loginstyle} onSelect={this.handleOpenLoginModal}>Choose identity</NavItem>
      <NavItem key={4} style={userstyle} href="#" disabled={true}>User {(this.state.identity || "").slice(5, 12)}</NavItem>
      <NavItem key={5} style={userstyle} href="#" onSelect={this.handleLogout}>Log out</NavItem>
      </Nav>
    );
  }
});

var SearchForm = React.createClass({
  mixins: [RoutingContextMixin],

  handleSearch: function (e) {
    var value = this.refs.search.getInputDOMNode().value;
    console.log ("Got new value here", value);
    var place = this.getRouting().makeHref("/search", { wikipage: value });
    window.location = place;
    e.preventDefault();
    e.stopPropagation();
  },

  href: function () {
    return this.getRouting().makeHref("/searchq", {});
  },

  render: function () {
    return this.transferPropsTo(
      <form action={this.href()} className="navbar-form navbar-left" method="GET">
        <Input type="text" role="search" ref="search" name="q" placeholder="Search..." />
        <Button onClick={this.handleSearch}><Glyphicon glyph="search" /></Button>
      </form>);
  }
});

var Header = module.exports = React.createClass({
  mixins: [LinkMixin],

  propTypes: {
    initialIdentity: React.PropTypes.string
  },

  render: function () {
    return (
      <Navbar role="navigation" navExpanded={true} fluid={true}>
      <Nav>
      <a className="navbar-brand" navbar={true}>Wiki</a>
      <NavItem key="edit" to="/create">Create page</NavItem>
      </Nav>

      <SearchForm />

      <UserNav initialIdentity={this.props.initialIdentity} />
      </Navbar>
    );
  }
});
