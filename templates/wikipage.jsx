/** @jsx React.DOM */
var React = require('react')
  , ReactAsync = require('react-async')
  , bs = require('react-bootstrap')
  , marked = require('marked')
  , superagent = require('superagent')
  , WikiContentLoaderMixin = require('./wikiContentLoaderMixin')
  , WikiTabs = require('./wikitabs.jsx')
  , ClaimGuardMixin = require('./claimguardmixin.jsx')
;


module.exports = React.createClass({
  mixins: [ReactAsync.Mixin, WikiContentLoaderMixin, ClaimGuardMixin],

  propTypes: {
    component: React.PropTypes.component,
    componentProps: React.PropTypes.object
  },

  getDefaultProps: function () {
    return {
      component: React.DOM.div,
      componentProps: {}
    };
  },

  getInitialStateAsync: function (cb) {
    this.getWikiContent (this.props.wikipage, cb);
  },

  componentDidMount: function () {
    console.log ("I am in the ", window ? "browser" : "server", " and I mounted with state", this.state);
  },

  renderView: function () {
    if (!this.state.value)
      this.state.value = "Loading";

    if (!this.state.title)
      this.state.title = "Loading";

    return this.props.component(this.props.componentProps, (<div>
        <WikiTabs wikipage={this.props.wikipage} tab="view" />
	<h1>{this.state.title}</h1>
        <h3><small>{this.props.wikipage} ({this.state.type||""})</small></h3>
        <div dangerouslySetInnerHTML={{__html: marked(this.state.value)}} />
      </div>));
  }
});
