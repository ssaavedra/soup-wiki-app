/** @jsx React.DOM */
var React = require('react')
  , bs = require('react-bootstrap');

var Nav = bs.Nav,
    NavItem = require('./NavItem'),
LinkMixin = require('rrouter/lib/LinkMixin');


var WikiTabs = module.exports = React.createClass({
  handleSelect: function (e) {
    console.log (arguments);
  },

  render: function () {
    if (typeof window!=="undefined" && !!window) { window.XX = this; }
    return (
      <Nav bsStyle="tabs" activeKey={this.props.tab} onSelect={this.handleSelect}>
      <NavItem key="view" to="/view" wikipage={this.props.wikipage}>Article</NavItem>
      <NavItem key="edit" to="/edit" wikipage={this.props.wikipage}>Edit</NavItem>
      <NavItem key="history" to="/history" wikipage={this.props.wikipage}>Show history</NavItem>
      </Nav>
    );
  }
});
