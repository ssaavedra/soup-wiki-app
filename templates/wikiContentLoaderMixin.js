var React = require('react'),
    superagent = require('superagent');

var WikiContentLoaderMixin = {
  contextTypes: {
    ext_routes: React.PropTypes.object
  },

  getExternalRoute: function (namespace) {
    var ext_routes = this.context
                  && this.context.ext_routes
                  || window.INITIAL_PROPS.ext_routes;

    return ext_routes[namespace];
  },

  getWikiContent: function (wikipage, cb) {
    var url = this.getExternalRoute('app.wikita.api.v1.get').replace(':1', wikipage);
    console.log ("Got Route ", url, " to ask for wikipage ", wikipage);

    var on_end = function (res) {
      if (res.ok){
	cb (undefined, { status: res.body.status,
                         value: res.body.text,
                         title: res.body.title,
                         type: res.body.type,
                         data: res.body });
      } else {
        cb (undefined, { status: res.body.status,
                         value: res.body.text,
                         title: res.body.title,
                         results: res.body.results,
                         type: res.body.type });
      }
    }

    var chain = superagent.get(url)
                .set('Accept', 'text/plain')
                .end(on_end);
  },

  getWikiHistory: function (wikipage, cb) {
    var url = this.getExternalRoute('app.wikita.api.v1.history').replace(':1', wikipage);
    var on_end = function (res) {
      console.log ("I got response from history", res);
          if (res.ok) {
            cb (undefined, { status: res.body.status,
                             permanode: res.body.permanode,
                             claims: res.body.claims
                           });
          } else {
            cb (undefined, { status: res.body.status,
                             permanode: res.body.permanode,
                             claims: res.body.claims
                           });
          }
    }

    console.log ("I'm asking for url", url);

    superagent.get (url)
    .set ('Accept', 'application/json')
    .end (on_end);
  },

  setWikiContent: function (wikipage, wikicontent, cb)
  {
    superagent.post(this.getExternalRoute('app.wikita.api.v1.edit')
                    .replace(':1', wikipage))
    .set('Accept', 'text/plain,application/json')
    .type('json')
    .send({content: wikicontent})
    .end(cb);
  },

  setWikiTitle: function (wikipage, wikititle, cb)
  {
    superagent.post(this.getExternalRoute('app.wikita.api.v1.edit.title').replace(':1', wikipage))
    .set('Accept', 'text/plain,application/json')
    .type('json')
    .send({title: wikititle})
    .end(cb);
  },

  setClaimSignature: function (claim_id, cb)
  {
    superagent.post(this.getExternalRoute('app.wikita.api.v1.sign-claim')
                    .replace(':1', claim_id))
    .set('Accept', 'text/plain,application/json')
    .type('json')
    .send({ claim: claim_id })
    .end(cb);
  },

  newWikiContent: function (wikipage, wikicontent, cb)
  {
    console.log ("I am about to POST ", this.getExternalRoute('app.wikita.api.v1.new'), {title: wikipage, content: wikicontent});
    superagent.post(this.getExternalRoute('app.wikita.api.v1.new'))
    .set('Accept', 'text/plain,application/json')
    .type('json')
    .send({
      title: wikipage,
      content: wikicontent
    })
    .end(cb);
  }
};

module.exports = WikiContentLoaderMixin;
