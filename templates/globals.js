/* Globals stuff that works everywhere */

if (typeof window !== "undefined" && !!window) {
  if (typeof "window.GLOBALS" !== "object")
    window.GLOBALS = {};

  module.exports = window.GLOBALS;
} else {
  module.exports = {};
}
