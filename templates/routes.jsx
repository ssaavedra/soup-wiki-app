/** @jsx React.DOM */
var React = require('react')
  , Preloaded = require('react-async').Preloaded
  , bs = require('react-bootstrap')
  , RRouter = require('rrouter')
  , Routes = RRouter.Routes
  , Route = RRouter.Route

  , WikiPage = require('./wikipage.jsx')
  , SearchResults = require('./searchresults.jsx')
  , NewPage = require('./newpage.jsx')
  , EditWikiPage = require('./editwikipage.jsx')
  , HistoryWikiPage = require('./historywikipage.jsx')
  , NotFoundPage = require('./404.jsx')
;

var pl = React.createClass({
  render: function () {
    return (<div></div>);
  }
});

var PL = function (component) {
  return function () {
    return Preloaded({preloader:pl(), alwayUsePreloader:true}, component.apply(this, arguments));
  }
};

PL = function (component) {
  return component;
};


module.exports = (
  <Routes>
  <Route path="/" view={PL(WikiPage)} name="main" />
  <Route path="/_new" view={PL(NewPage)} name="create" />
  <Route path="/:wikipage" view={PL(WikiPage)} name="view" />
  <Route path="/_edit/:wikipage" view={PL(EditWikiPage)} name="edit" />
  <Route path="/_search/:wikipage" view={PL(SearchResults)} name="search" />
  <Route path="/_search" view={pl} name="searchq" />
  <Route path="/_history/:wikipage" view={PL(HistoryWikiPage)} name="history" />
  </Routes>
);
