/** @jsx React.DOM */
var React = require('react')
  , ReactAsync = require('react-async')
  , bs = require('react-bootstrap')
  , marked = require('marked')
  , superagent = require('superagent')
  , WikiContentLoaderMixin = require('./wikiContentLoaderMixin')
  , WikiTabs = require('./wikitabs.jsx');

var ButtonToolbar = bs.ButtonToolbar
  , Button = bs.Button
  , Input = bs.Input
;


module.exports = React.createClass({
  mixins: [WikiContentLoaderMixin],

  propTypes: {
    component: React.PropTypes.component,
    componentProps: React.PropTypes.object
  },

  getDefaultProps: function () {
    return {
      component: React.DOM.div,
      componentProps: {}
    };
  },

  getInitialState: function () {
    return {
      value: ""
    };
  },

  handleChange: function () {
    this.setState ({value: this.refs.textarea.getInputDOMNode().value});
  },

  handleTitleChange: function () {
    this.setState ({title: this.refs.title.getInputDOMNode().value});
  },

  handleSubmit: function (e) {
    this.newWikiContent (this.state.title, this.state.value, function (x) {
      // Redirect to created page!
      window.location = window.location + "/../" + x.body.location;
    });

    console.log ("Should submit now, but cannot", e);
    console.log ("Our new content is", this.state.value);

    e.preventDefault();
  },

  componentDidMount: function () {
    console.log ("I am in the ", window ? "browser" : "server", " and I mounted with state", this.state);
  },

  render: function () {
    return this.props.component(this.props.componentProps, (
      <div className="MarkdownEditor">
        <h1>Create new wiki page ({this.state.title})</h1>
      <form role="form" action="/app/wiki/_new" method="post">

      <div className="form-group">
      <Input type="text" label="Title of the wiki page" ref="title" placeholder="Insert title..." onChange={this.handleTitleChange} />
        <Input type="textarea"
          onChange={this.handleChange}
          ref="textarea"
          value={this.state.value} />

      </div>

        <ButtonToolbar>
          <Button onClick={this.handleSubmit}>Create</Button>
        </ButtonToolbar>
      </form>
        <h3>Preview</h3>
        <div
          className="content"
          dangerouslySetInnerHTML={{__html: marked(this.state.value || "")}} />
      </div>));
  }
});
