/** @jsx React.DOM */
var React = require('react')
  , ReactAsync = require('react-async')
  , bs = require('react-bootstrap')
  , WikiTabs = require('./wikitabs.jsx')
  , WikiContentLoaderMixin = require('./wikiContentLoaderMixin')
  , ClaimGuardMixin = require('./claimguardmixin.jsx')
;

var THEGLOBALS = require('./globals');


var render_claims_list = function (l, cur_identity, signClaimHandler) {
    console.log (l);

    var lis = l.sort(function (a, b) {
	if (a.content.date > b.content.date) {
	    return -1;
	}
	if (a.content.date < b.content.date) {
	    return 1;
	}
	return 0;
    }).map (function (item) {
      var id = item._id,
      attribute = item.content.sub.attribute, a, desc;

      if (attribute === "content") {
	a = React.DOM.a ({
	  href: "../" + id
	}, id.substring (0, 12));

	desc = "";

      } else {
	a = React.DOM.a ({
	  href: "#",
	  disabled: true
	}, id.substring (0, 12));

	if (attribute === "title") {
	  desc = " to \"" + item.content.sub.value + "\"";
	}
      }

      var signatures = item.meta.signatures;
      var signed = signatures.some(function (sig) { return sig.issuer === cur_identity });
      var sig_resume = bs.OverlayTrigger({
	placement: "top",
	overlay: bs.Popover(null,
	  React.DOM.ul.bind (null, null).apply(null, signatures.map (function (sig) {
	    return React.DOM.li (null, sig.issuer.substring(5, 12));
	  })))
      }, React.DOM.span(null, " " + signatures.length + " signature(s)"));

      var sig_btn = null;
      var noident = cur_identity == null;
      // TODO:
      sig_btn = bs.Button({disabled: (signed || noident) && "disabled" || false, onClick: signClaimHandler(id)}, (signed ? "Already signed" : (noident ? "Login before signing" : "Sign")));

	return React.DOM.li (null, a, " changes ", attribute, desc, " at ", item.content.date, sig_resume, sig_btn);
    });

    return React.DOM.ul.apply (null, [null].concat (lis));
};


module.exports = React.createClass({
    mixins: [ReactAsync.Mixin, WikiContentLoaderMixin, ClaimGuardMixin],

  contextTypes: {
    identity: React.PropTypes.string
  },


  getInitialStateAsync: function (cb) {
    this.getWikiHistory (this.props.wikipage, cb);
  },

  signClaimHandler: function (claim_id) {
    return (function (e) {
      return this.signClaim (claim_id);
    }.bind (this));
  },

  signClaim: function (claim_id) {
    this.setClaimSignature(claim_id,
      function (xhr) {
	this.getInitialStateAsync (function (ok, nextState) {
	  this.setState (nextState);
	}.bind(this));
      }.bind(this));
  },

  renderView: function () {
    var claims, permanode_link;
    claims = [];
    permanode_link = "";

    if (this.state !== null &&
	this.state.claims &&
	this.state.permanode) {

      claims = this.state.claims || [];
      permanode_link = React.DOM.a ({
	href: "../" + this.state.permanode
      }, this.state.permanode.substring (0, 12));
    }

    return (
      <div>
      <WikiTabs wikipage={this.props.wikipage} tab="history" />
      <h1>History of {this.state.permanode||"..."}</h1>
      <p>Latest version available at: {permanode_link}</p>
      <div>{render_claims_list(claims, THEGLOBALS.identity, this.signClaimHandler)}</div>
      </div>);
  }
});
