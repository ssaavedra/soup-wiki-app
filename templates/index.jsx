/** @jsx React.DOM */
var React = require('react')
  , bs = require('react-bootstrap');

var Navbar = bs.Navbar
  , Nav = bs.Nav
  , NavItem = bs.NavItem;

var Base = require(__dirname + '/base.jsx');

var NavItem = require('react-bootstrap').NavItem;

var Index = module.exports = React.createClass({
  render: function () {

    var handleSelect = function (a) {
      console.log ("Hello", a);
    };

    return (<Base><div><h1>{this.props.page_title}</h1>
      <Nav bsStyle="tabs" activekey={1} onSelect={handleSelect}>
        <NavItem key={1} href="/page">Wiki page</NavItem>
        <NavItem key={2} href="/_edit/page">Edit</NavItem>
        <NavItem key={3} href="/_history/page">History</NavItem>
      </Nav>
      <p>{this.props.page_content}</p>
      </div>
      </Base>);
  }
});
