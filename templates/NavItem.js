/** @jsx React.DOM */

var React = require('react');
var classSet = require('react-bootstrap/utils/classSet');
var BootstrapMixin = require('react-bootstrap/BootstrapMixin');
var LinkMixin = require('rrouter/lib/LinkMixin');

var NavItemLink = React.createClass({displayName: 'NavItemLink',
  mixins: [BootstrapMixin, LinkMixin],

  propTypes: {
    onSelect: React.PropTypes.func,
    active: React.PropTypes.bool,
    disabled: React.PropTypes.bool,
    href: React.PropTypes.string,
    title: React.PropTypes.string
  },

  render: function () {
    var classes = {
      'active': this.props.active,
      'disabled': this.props.disabled
    };

    return React.DOM.li({className:classSet(classes)},
			this.transferPropsTo(
			  React.DOM.a(
			    { href:this.href(),
			      title:this.props.title,
			      onClick:this.handleClick,
			      ref:"anchor"
			    }, this.props.children
			  )
			)
		       );
  },

  handleClick: function (e) {
    if (this.props.onSelect) {
      e.preventDefault();

      if (!this.props.disabled) {
        this.activate();
        this.props.onSelect(this.props.key,this.props.href);
      }
    }
  }
});

module.exports = NavItemLink;
