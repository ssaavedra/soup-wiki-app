/** @jsx React.DOM */
var React = require('react');

var Error404 = module.exports = React.createClass({
  render: function () {
    return (<div><h1>Error 404</h1>
      <p>Could not find the required path: {this.props.path}</p>
      </div>);
  }
});

