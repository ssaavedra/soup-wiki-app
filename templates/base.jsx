/** @jsx React.DOM */

var React = require('react');
var Router = require('rrouter');

var Routes = Router.Routes;
var Route = Router.Route;
var NotFound = Router.NotFound;

var Header = require('./header.jsx');

var WikiPage = require('./wikipage.jsx');
var EditWikiPage = require('./editwikipage.jsx');
var HistoryWikiPage = require('./historywikipage.jsx');
var NotFoundPage = require('./404.jsx');



var Root = module.exports = React.createClass({

  displayName: "Root",

  statics: {
    /**
     * Get the doctype the page expects to be rendered with
     *
     * @returns {string}
     */
    getDoctype: function () {
      return '<!DOCTYPE html>';
    },
    /**
     * Get the list of pages that are renderable
     *
     * @returns {Array}
     */
    getPages: function () {
      return [
	'index.html',
	'getting-started.html',
	'css.html',
	'components.html',
	'javascript.html'
      ];
    },
    renderToString: function (props) {
      var component;

      component = React.renderComponentToString(Root(props));

      return Root.getDoctype() +
      component;
    },
    PagesHolder: React.createClass({
      render: function () {
	return this.transferPropsTo(
	  <Locations>
          <Location path="/" view={WikiPage} />
          <Location path="/:wikipage" view={WikiPage} />
          <Location path="/_edit/:wikipage" view={EditWikiPage} />
          <Location path="/_history/:wikipage" view={HistoryWikiPage} />
          <NotFound view={NotFoundPage} />
	  </Locations>
	);
      }
    })
  },

  propTypes: {
    initialIdentity: React.PropTypes.string,
    ext_routes: React.PropTypes.object.isRequired
  },

  getDefaultProps: function () {
    return { initialIdentity: null };
  },

  /**
   * Get the Base url this app sits at
   * This url is appended to all app urls to make absolute url's within the app.
   *
   * @returns {string}
   */
  getBaseUrl: function () {
    return this.props.prefix;
  },

  render: function () {
    // Dump out our current props to a global object via a script tag so
    // when initialising the browser environment we can bootstrap from the
    // same props as what each page was rendered with.

    // console.log ("THESE ARE MY PROPS", this.props);

    var browserInitScriptObj = {
      __html:
      "window.INITIAL_PROPS = " + JSON.stringify({
	initialPath: this.props.initialPath,
	prefix: this.props.prefix,
	path: this.props.path,
	initialIdentity: this.props.initialIdentity,
	ext_routes: this.props.ext_routes
      }) + ";\n" +
      // console noop shim for IE8/9
      "(function (w) {\n" +
      " var noop = function () {};\n" +
      " if (!w.console) {\n" +
      " w.console = {};\n" +
      " ['log', 'info', 'warn', 'error'].forEach(function (method) {\n" +
      " w.console[method] = noop;\n" +
      " });\n" +
      " }\n" +
      "}(window));\n"
    };
    var head = {
      __html: '<title>Wikita: A Sample Wiki application</title>' +
      '<meta http-equiv="X-UA-Compatible" content="IE=edge" />' +
      '<meta name="viewport" content="width=device-width, initial-scale=1.0" />' +
      '<link href="' + this.getBaseUrl() + '/static/css/bootstrap.css" rel="stylesheet" />' +
      '<link href="' + this.getBaseUrl() + '/static/css/style.css" rel="stylesheet" />' +
      '<!--[if lt IE 9]>' +
      '<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>' +
      '<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>' +
      '<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>' +
      '<script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/3.4.0/es5-shim.js"></script>' +
      '<script src="http://cdnjs.cloudflare.com/ajax/libs/es5-shim/3.4.0/es5-sham.js"></script>' +
      '<![endif]-->'
    };


    var containerStyle = {
      maxWidth: "980px",
      margin: "auto"
    };

    var contentStyle = {
      maxWidth: "900px",
      margin: "auto"
    };



    return (
      <html>
      <head dangerouslySetInnerHTML={head} />
      <body>
      <div id="container" style={containerStyle}>
      <div id="header">
      <Header initialIdentity={this.props.initialIdentity} />
      </div>

      We are in the route "{this.getBaseUrl()}" + {this.props.path}

      <br /><br />

      <main>
      {this.props.children}
      </main>

      </div>
      <script dangerouslySetInnerHTML={browserInitScriptObj} />
      </body>
      </html>
    );
  }
});
