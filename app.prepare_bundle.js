var browserify = require('browserify');
var fs = require('fs');

var bundle_location = __dirname + "/client_bundle.cached.js";

var b = browserify()
        .add(__dirname + '/client')
        .bundle(function(err, buf) {
          if (err) {
            console.log ("Error happened", err);
            return;
          }
          fs.writeFile(bundle_location, buf, function (err) {
            console.log ("Bundle updated", err);
          });
});

setInterval(function(){
  console.log ("Still alive");
}, 10000);
