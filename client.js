'use strict';

var React = require('react');
var RRouter = require('rrouter');

var routes = require('./templates/routes.jsx');
var Root = require('./templates/base.jsx');

window.React = React;

var dumbRoute = React.createClass({
  render: function () { return React.DOM.div(null, "This is dumb stuff"); }
});

var Routes = RRouter.Routes, Route = RRouter.Route;

React.withContext({ext_routes: window.INITIAL_PROPS.ext_routes}, function () {

  var client_routes = Routes(null,
                             Route({path: "/app/wiki"}, // window.INITIAL_PROPS.prefix},
                                   routes));

  RRouter.start (client_routes, function (view, match) {

    console.log ("Path is ", match, view);
    React.renderComponent(Root(window.INITIAL_PROPS, view), document);
  });

/*
React.renderComponent(
Root({ path: match.path,
initialPath: window.INITIAL_PROPS.initialPath,
prefix: window.INITIAL_PROPS.prefix
}, view),
document);
*/

});


// React.renderComponent(Root(window.INITIAL_PROPS), document);
